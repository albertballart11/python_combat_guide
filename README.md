# python_combat_guide

Those are sample codes for the Python manual Python Combat Guide, by Carles Mateo.
https://blog.carlesmateo.com

Those samples are free for personal use, learning and for Open Source projects.
Optionally if you like it you can donate.
If you're a company and this information has been useful and productive, donations are welcome.

No commercial use is allowed, reprinting, or charging for the book and these contents.

You can buy the book in here: [https://leanpub.com/pythoncombatguide](https://leanpub.com/pythoncombatguide)