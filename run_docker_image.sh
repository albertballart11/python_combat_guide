#!/bin/bash

DOCKER_IMAGE_NAME="python_combat_guide"

printf "Removing old image %s\n" "${DOCKER_IMAGE_NAME}"
sudo docker rm "${DOCKER_IMAGE_NAME}"

echo "Creating Docker Image"
sudo docker build -t ${DOCKER_IMAGE_NAME} . --no-cache

retVal=$?
if [ $retVal -ne 0 ]; then
    printf "Error. Exit code %s\n" ${retVal}
    exit
fi

echo "Running Docker Container"
sudo docker run --name ${DOCKER_IMAGE_NAME} ${DOCKER_IMAGE_NAME}
