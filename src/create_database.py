#!/usr/bin/env python3
#
# Creates the Database required for Storing Disk information
#
# Author: Carles Mateo
# Creation Date: 2017-06-10
# Description: Creates the File Database and the Table ‘disk_status’
#

from lib.db import Db


def create_table(s_table_name, s_sql_write_sqlite):
    print("Creating... " + s_table_name)
    b_result, s_error_message = o_db.db_query_write(s_sql_write_sqlite)
    if b_result is False:
        print("Creation failed for: " + s_table_name)
        print(s_error_message)
    else:
        print("Creation successful for: " + s_table_name)


s_db_filename = "system_information.db"

# For MySQL
s_sql_disk_status_write_mysql = """CREATE TABLE 'disk_status' (
                    's_id_server' VARCHAR(20) NOT NULL,
                    's_id_wwn' VARCHAR(30) NOT NULL,
                    's_datetime' VARCHAR(19) NOT NULL,
                    's_status' VARCHAR(20) NOT NULL,
                    's_id_serial' VARCHAR(30) NOT NULL,                    
                    's_size' VARCHAR(20) NOT NULL,
                    's_name' VARCHAR(20) NOT NULL,
                    's_logical_size' VARCHAR(10) NOT NULL,
                    's_physical_size' VARCHAR(10) NOT NULL,
                    's_slot' VARCHAR(3) NOT NULL,
                    's_temperature' VARCHAR(5) NOT NULL,
                    's_role' VARCHAR(20) NOT NULL,
                    PRIMARY KEY ('s_id_wwn', 's_datetime', 's_status')
                )
                COLLATE='latin1_general_ci';
"""

# For SQLite3
s_sql_disk_status_write_sqlite = """CREATE TABLE 'disk_status' (
                    's_id_server' VARCHAR(20) NOT NULL,
                    's_id_wwn' VARCHAR(30) NOT NULL,
                    's_datetime' VARCHAR(19) NOT NULL,
                    's_status' VARCHAR(20) NOT NULL,
                    's_id_serial' VARCHAR(30) NOT NULL,                    
                    's_size' VARCHAR(20) NOT NULL,
                    's_name' VARCHAR(20) NOT NULL,
                    's_logical_size' VARCHAR(10) NOT NULL,
                    's_physical_size' VARCHAR(10) NOT NULL,
                    's_slot' VARCHAR(3) NOT NULL,
                    's_temperature' VARCHAR(5) NOT NULL,
                    's_role' VARCHAR(20) NOT NULL,
                    PRIMARY KEY ('s_id_wwn', 's_datetime', 's_status')
                );
"""

s_sql_servers_write_sqlite = """CREATE TABLE 'servers' (
                    's_id_server' VARCHAR(20) NOT NULL,  
                    's_dns' VARCHAR(30) NOT NULL,
                    's_creation_datetime' VARCHAR(19) NOT NULL,
                    's_status' VARCHAR(20) NOT NULL,
                    's_username' VARCHAR(10) NOT NULL,                    
                    's_password' VARCHAR(14) NOT NULL,
                    's_role' VARCHAR(20) NOT NULL,
                    PRIMARY KEY ('s_dns', 's_datetime', 's_status')
                );
"""

o_db = Db(s_db_filename, s_db_type=Db.TYPE_SQLITE3)

create_table("disk_status", s_sql_disk_status_write_sqlite)
create_table("servers", s_sql_servers_write_sqlite)
