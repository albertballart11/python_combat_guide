#!/usr/bin/env python3
#
# serialize.py
#
# Author: Carles Mateo
# Creation Date: 2017
# Description: Sample file to show the use of serialization.
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

import ast

a_excluded = ["Desktop/", "Downloads/"]

s_excluded = repr(a_excluded)

print("The original Array object:")
print(a_excluded)

print("Serialized Array excluded:")
print(s_excluded)

# deserialize
a_excluded_reloaded = ast.literal_eval(s_excluded)
print("Deserialized back Array excluded:")
print(a_excluded_reloaded)

h_excluded = {"exluded": [{"name": "Desktop/",
                           "date_excluded": "2020-10-10"},
                          {"name": "Downloads/",
                           "data_excluded": "2020-10-09"}]}

s_excluded = repr(h_excluded)

print("The original Array Hash object:")
print(h_excluded)

print("Serialized Array excluded:")
print(s_excluded)

# deserialize
h_excluded_reloaded = ast.literal_eval(s_excluded)
print("Deserialized back Array excluded:")
print(h_excluded_reloaded)
