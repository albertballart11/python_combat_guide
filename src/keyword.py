#
# keyword.py
#
# Author: Carles Mateo
# Creation Date: 2020-04-28 18:50 GMT+1
# Description: How the keyword arguments work
#


class GameAdder:

    h_games = {}

    def add_game(self, s_game_name="Unknown", nargs=None, **kwargs):
        print("Preparing parameters for: " + s_game_name)
        print("-"*40)
        print(kwargs)
        print("-" * 40)
        print("Filtering out Private Variables")
        print("-" * 40)
        for s_key in kwargs:
            if s_key[0] != "_":
                if s_game_name in self.h_games:
                    self.h_games[s_game_name][s_key] = kwargs[s_key]
                else:
                    self.h_games[s_game_name] = {}
                    self.h_games[s_game_name][s_key] = kwargs[s_key]

            else:
                print("Private (internal) property: " + s_key)

    def get_games(self):
        return self.h_games


o_ga = GameAdder()
o_ga.add_game(s_game_name="Wow", _release_ver = "7.1050", _release_date = "2020-04-29", s_nice_name = "World of Warcraft")
o_ga.add_game(s_game_name="OW", _release_ver = "7.7777", _release_date = "2018-01-10", _responsible = "Carles Mateo", i_price = 35)
o_ga.add_game(s_game_name="SC2", _release_ver = "5.1234", _release_date = "2010-02-01", s_responsible = "Carles Mateo", i_price = 50, b_is_free=True)

print("\n")
print("Games")
print("=====")
print(o_ga.h_games)
