#
# subprocesses.py
#
# Author: Carles Mateo
# Creation Date: 2014
# Description: Deals with executing calls to system
#

import subprocess


class SubProcesses:

    def execute_command_for_output(self, s_command, b_shell=True, b_convert_to_ascii=True):
        """
        Executes Command and returns the Error Code, the Standard Output STDOUT and Standard Error STDERR
        """

        s_output = ""
        s_error = ""

        try:
            # Note: In Python3 this will return a binary
            process = subprocess.Popen([s_command],
                                       shell=b_shell,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
            s_output, s_error = process.communicate()
            i_error_code = process.returncode

        except:
            i_error_code = 99

        if b_convert_to_ascii is True:
            s_output = s_output.decode('ascii')
            s_error = s_error.decode('ascii')

        return i_error_code, s_output, s_error

