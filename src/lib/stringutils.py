#
# stringutils.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: StringUtils class, to deal with string and number formatting
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

class StringUtils:

    def get_format_string_from_number(self, f_number):
        """
        Returns a string formatted, with 2 decimal places from a float
        :param f_number: Float or int number
        :return: str: A string formatted
        """

        s_nice_number = "{0:.2f}".format(f_number)

        return s_nice_number
