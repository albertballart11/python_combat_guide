#
# sleeputils.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: SleepUtils Demo class to show Fixtures on time.sleep
# URL: https://gitlab.com/carles.mateo/python_combat_guide/-/blob/master/src/lib/sleeputils.py
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#


import time

class SleepUtils:

    def rest(self, i_seconds):
        time.sleep(i_seconds)
