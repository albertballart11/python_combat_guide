#
# Args Utils
#
# Author: Carles Mateo
# Creation Date: 2005
# Last Update: 2020-07-23 10:38 Ireland
# Description: Class to deal with Command Line Arguments
#


class ArgsUtils:
    # Original List of Args passed to the Constructor
    a_s_args = []
    # Dictionary produced format
    d_s_args = {}

    def __init__(self, a_s_args):
        """Get the arguments from Command Line and returns a dictionary with them

        :type: list[str]
        """
        d_s_args = {}
        i_num_args = 0

        self.a_s_args = a_s_args

        try:
            i_index = 0
            for s_arg in a_s_args:
                # Skip the own filename in index 0
                if i_index > 0:
                    i_num_args = i_num_args + 1
                    st_arg_pair = s_arg.split("=")
                    # NOTE: Please be aware that this will also deal with parameters like -p=1=2
                    #       And will return {'-p'} = "1=2"
                    if len(st_arg_pair) > 1:
                        d_s_args[st_arg_pair[0]] = "=".join(st_arg_pair[1:])
                    else:
                        d_s_args[st_arg_pair[0]] = ""

                i_index = i_index + 1

        except:
            # Clean the list of args if something went wrong
            d_s_args = {}

        self.d_s_args = d_s_args

    def get_arg(self, s_arg):
        """
        Returns the argument's value
        :type: str
        :rtype: boolean
        :rtype: str
        """

        s_value = ""
        b_exist = False

        if s_arg in self.d_s_args:
            s_value = self.d_s_args[s_arg]
            b_exist = True

        return b_exist, s_value

    def get_arg_from_list(self, a_s_args, s_default=""):
        """
        Returns the first argument's value, from a list of them, or a default value
        :type: Array of str
        :rtype: boolean
        :rtype: str
        """

        s_value = s_default
        b_exist = False

        for s_arg in a_s_args:
            if s_arg in self.d_s_args:
                s_value = self.d_s_args[s_arg]
                b_exist = True

                break

        return b_exist, s_value

    def get_dictionary_of_args(self):
        """
        Returns the produced dictionary
        :rtype: dict
        """

        return self.d_s_args

    def get_list_of_args(self):
        """
        Returns the original list of args passed
        :rtype: list
        """

        return self.a_s_args
