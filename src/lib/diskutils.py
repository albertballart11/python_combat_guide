#
# diskutils.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: DiskUtils class to teach new advance on testing
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

import os


class DiskUtils:

    def get_total_and_free_space_in_bytes(self, s_mountpoint="/"):
        """
        Return the size of the / mountpoint and the freespace
        :param s_mountpoint: String with the mountpoint
        :return: bool: b_success
        :return: int: i_total_size
        :return: int: i_free_size
        """
        b_success = True
        i_total_size = 0
        i_free_size = 0
        try:
            o_result = os.statvfs(s_mountpoint)
            i_block_size = o_result.f_frsize
            i_total_blocks = o_result.f_blocks
            i_free_blocks = o_result.f_bfree

            i_total_size = i_total_blocks * i_block_size
            i_free_size = i_free_blocks * i_block_size
        except:
            b_success = False

        return b_success, i_total_size, i_free_size

    def get_free_space_in_multiple_units(self, s_mountpoint="/"):
        """
        Will return the Free Space in / in multiply units.
        :param s_mountpoint: String with the mountpoint
        :return: bool: b_success
        :return: int: i_free_size_in_bytes
        :return: float: f_free_size_in_kbytes
        :return: float: f_free_size_in_mbytes
        :return: float: f_free_size_in_gbytes
        :return: float: f_free_size_in_tbytes

        """
        f_free_size_in_kbytes = 0
        f_free_size_in_mbytes = 0
        f_free_size_in_gbytes = 0
        f_free_size_in_tbytes = 0
        b_success, i_total_size_in_bytes, i_free_size_in_bytes = self.get_total_and_free_space_in_bytes(s_mountpoint)
        if b_success is True:
            f_free_size_in_kbytes = i_free_size_in_bytes / 1024
            f_free_size_in_mbytes = f_free_size_in_kbytes / 1024
            f_free_size_in_gbytes = f_free_size_in_mbytes / 1024
            f_free_size_in_tbytes = f_free_size_in_gbytes / 1024

        return b_success, \
               i_free_size_in_bytes, \
               f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes
