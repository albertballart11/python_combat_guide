#
# Database support
#
# Author: Carles Mateo
# Creation Date: 2017-06-01
# Description: Class to work with Databases.
#              This is a simplified version to work with SQLite3 only
#

import sqlite3


class Db:
    # Database Filename
    s_db_filename = ""
    b_db_loaded = False
    o_db_conn = None

    TYPE_MYSQL = "MYSQL"
    TYPE_SQLITE3 = "SQLITE3"

    def __init__(self, s_db_filename="", s_db_type=TYPE_SQLITE3):
        """
        Constructor.
        Initializes the Connection
        :param s_db_filename: Name of the file holding the SQLite3 Database
        """
        self.s_db_filename = s_db_filename
        # Attempt to Connect
        self.b_db_loaded = self.db_connect()

    def db_connect(self):
        """
        Connects to the Database Filename.
        If it doesn't exist, the file will be created.
        :return: Boolean: If connection was success or not
        """

        try:

            # First attempt to close if there was a connection open
            b_success_closing = self.db_close()

            self.o_db_conn = sqlite3.connect(self.s_db_filename)

            if self.o_db_conn is None:
                self.b_db_loaded = False
            else:
                self.b_db_loaded = True
        except sqlite3.DatabaseError as e:
            self.b_db_loaded = False
            self.o_db_conn = None
        except:
            self.b_db_loaded = False
            self.o_db_conn = None

        return self.b_db_loaded

    def db_close(self):
        """
        Attempts to close the Connection
        :return: Boolean: If operation went well without errors
        :return: String: Error message if any
        """

        b_success = True
        s_error_message = ""

        try:
            if self.o_db_conn is not None:
                self.o_db_conn.close()
                self.b_db_loaded = False
                self.o_db_conn = None
        except sqlite3.DatabaseError as e:
            b_success = False
            self.b_db_loaded = False
            self.o_db_conn = None
            s_error_message = str(e)
        except:
            # problems closing the Database. Perhaps was not in use
            b_success = False
            self.b_db_loaded = False
            self.o_db_conn = None

        return b_success, s_error_message

    def db_query_read(self, s_sql):
        """
        Execute a Query for READ using a cursor. Returns a List
        :param s_sql: The Query
        :return: Boolean: Success
        :return: String: Error message
        :return: List: Rows
        """
        b_success = True
        s_error_message = ""
        a_results = {}
        try:
            a_results = self.o_db_conn.cursor().execute(s_sql).fetchall()
        except sqlite3.OperationalError as e:
            # In msg there is an error message
            s_error_message = str(e)
            b_success = False
        except:
            b_success = False

        return b_success, s_error_message, a_results

    def db_query_write(self, s_sql_write):
        """
        Executes a Query for WRITE (Insert, Update, Alter, CREATE TABLE...)
        Commits after, in order to make the changes visible to other Connections/Instances
        :param s_sql_write: The Query
        :return: Boolean: True for Success, False for Error
        :return: String : Error Message
        """
        b_success = True
        s_error_message = ""
        try:
            self.o_db_conn.execute(s_sql_write)
            self.o_db_conn.commit()
        except sqlite3.OperationalError as e:
            # In msg there is an error message
            s_error_message = str(e)
            b_success = False
        except:
            b_success = False

        return b_success, s_error_message

    def __del__(self):
        """
        # The destructor. Attempt to close the connection
        """
        b_success = self.db_close()
