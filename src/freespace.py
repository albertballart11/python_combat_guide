#!/usr/bin/env python3
#
# freespace.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: Sample file to show the tests. It returns the free space in different units
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

from lib.diskutils import DiskUtils
from lib.stringutils import StringUtils


class FreeSpace:

    o_diskutils = None
    o_stringutils = None

    def __init__(self, o_diskutils, o_stringutils):
        self.o_diskutils = o_diskutils
        self.o_stringutils = o_stringutils

    def get_free_space_formatted(self, s_path="/"):

        b_success, \
        i_free_size_in_bytes, \
        f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes = \
            self.o_diskutils.get_free_space_in_multiple_units(s_path)

        if b_success is False:
            return b_success, "", "There has been an error reading " + s_path

        s_warnings = ""
        if f_free_size_in_tbytes >= 1:
            # Big drive with a lot of Free Space! We show the space in TB
            s_free_space = self.o_stringutils.get_format_string_from_number(f_free_size_in_tbytes)
            # Append the Units
            s_free_space = s_free_space + " TB"
        elif f_free_size_in_gbytes >= 1:
            s_free_space = self.o_stringutils.get_format_string_from_number(f_free_size_in_gbytes)
            # Append the Units
            s_free_space = s_free_space + " GB"
        elif f_free_size_in_mbytes >= 1:
            # Few space
            s_free_space = self.o_stringutils.get_format_string_from_number(f_free_size_in_mbytes)
            # Append the Units
            s_free_space = s_free_space + " MB"
            s_warnings = s_warnings + "You are really running out of space...\n"
        elif f_free_size_in_kbytes >= 1:
            # Really few space
            s_free_space = self.o_stringutils.get_format_string_from_number(f_free_size_in_kbytes)
            # Append the Units
            s_free_space = s_free_space + " KB"
            s_warnings = s_warnings + "You have only few Kbytes left!\n"
        else:
            # Only bytes left or 0 space
            s_free_space = str(i_free_size_in_bytes) + " Bytes"
            s_warnings = s_warnings + "You are actually really running out of any space\n"

        return b_success, s_free_space, s_warnings


# Main program
# Prepare dependencies
o_diskutils = DiskUtils()
o_stringutils = StringUtils()

# Launch
o_freespace = FreeSpace(o_diskutils, o_stringutils)
b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")

if b_success is True:
    print("Read successfully /")
    print("Free Space: " + s_free_space)
    if s_warnings != "":
        print("Warning: " + s_warnings)
