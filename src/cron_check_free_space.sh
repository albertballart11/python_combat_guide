#!/bin/bash

# Example of Cron file that sends an email if a Mountpoint is using more space than expected.

# Example of a cron file. It will send an email if disk space is full
ADMIN_EMAIL="your@email.com"

# If we use more than this space we should get a Warning
MAXSPACE_PCT=70

if [[ $# -eq 1 ]]; then
# We accept a parameter indicating what is the Warning threeshold
  MAXSPACE_PCT=$1
  if [ ${MAXSPACE_PCT} -gt 99 ] || [ ${MAXSPACE_PCT} -lt 1 ]; then
      echo "Max Space should be a percentage between 1 and 99"
      exit 1
  fi
fi

# Name of the machine
HOSTNAME=`hostname`

NUM_ALERTS=0
TEXT_ALERTS=""

for LINE in $(df -h | grep -v "/dev/loop" | awk '{ print $6 "%" $5; }')
do
    MOUNTPOINT=`echo $LINE | tr '%' ' ' | awk '{ print $1; }'`
    FREESPACE=`echo $LINE | tr '%' ' ' | awk '{ print $2; }'`

    if [[ $FREESPACE -gt ${MAXSPACE_PCT} ]]; then
        NUM_ALERTS=$((NUM_ALERTS+1))
        TEXT_ALERTS="${TEXT_ALERTS}Warning: $MOUNTPOINT is becoming full with ${FREESPACE}% in use\n"
    fi
done

# This will go to the Logs or the Standard Output
echo "${NUM_ALERTS} Disk Space Alerts found"
echo -e "${TEXT_ALERTS}"

if [[ ${NUM_ALERTS} -gt 0 ]]; then
    echo "Sending email to Admin ${ADMIN_EMAIL}"
    #echo -e "${TEXT_ALERTS}" | mail -s "Disk Space Alerts on $HOSTNAME" "${ADMIN_EMAIL}"
fi