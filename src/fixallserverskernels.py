#!/usr/bin/env python3
#
# fixallserverskernels.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: Sample file to show OOP and the ability to do Operations on Servers
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

import time
from server import Server

o_dns1 = Server("dns01", "10.0.10.7", "noc")
o_dns2 = Server("dns01", "10.0.10.8", "noc")
o_lb1 = Server("loadbalancer01", "10.5.2.101", "noc")
o_lb2 = Server("loadbalancer02", "10.5.2.102", "noc")
o_lb3 = Server("loadbalancer03", "10.5.2.103", "noc")
o_lb4 = Server("loadbalancer04", "10.5.2.104", "noc")
o_db1 = Server("mysql-db-01", "10.0.0.10", "noc")

# We select the ones we want to check and upgrade if needed
a_servers_to_upgrade = [o_dns1, o_lb1, o_lb3, o_lb4, o_db1]

i_errors = 0
i_tries = 0
b_do_update = True

while b_do_update is True:
    print("Starting to check Kernel for the Servers")
    for o_server in a_servers_to_upgrade:
        s_server_status = o_server.get_status()
        if s_server_status == Server.POWERED_ON:
            s_server_kernel = o_server.get_kernel_version()
            # Remove Enters at the end
            s_server_kernel = s_server_kernel.strip()

            if s_server_kernel != "4.15.0-91-generic":
                print("Not expected kernel version " + s_server_kernel + " in server: " + o_server.get_hostname() + ", fixing...")
                o_server.send_upgrade_kernel("linux-modules-extra-4.15.0-91-generic")
                print("Rebooting server: " + o_server.get_hostname())
                o_server.send_reboot()
                print("Waiting 120 seconds for the server to come back after reboot...")
                # sleep 2 minutes to give the Server time to reboot
                time.sleep(120)
                # Refresh the new Kernel info
                o_server.update_kernel_info()
                s_server_kernel = o_server.get_kernel_version()
                # Remove Enters at the end
                s_server_kernel = s_server_kernel.strip()

                if s_server_kernel != "4.15.0-91-generic":
                    print("Upgrading of the Kernel: " + s_server_kernel + " to " + "4.15.0-91-generic" + " failed")
                    i_errors = i_errors + 1
            else:
                print("Server " + o_server.get_hostname() + " has a good Kernel version")
        else:
            print("Server " + o_server.get_hostname() + " is in wrong status: " + s_server_status + " we cannot check")
            i_errors = i_errors + 1

    print("Found " + str(i_errors) + " errors trying to update")
    if i_errors == 0:
        # All done, exit the maintenance loop
        print("All Kernels are good, exiting")
        b_do_update = False
    else:
        i_tries = i_tries + 1
        if i_tries > 5:
            print("We tried 5 times and there are still Servers causing problems")
        else:
            # Sleep one minute before retrying
            time.sleep(60)
