#
# wow_instances.py
#
# Author: Carles Mateo
# Creation Date: 2020-04-28 18:50 GMT+1
# Description: It shows the scope for defining, it shows how a Mutable object will be persistent.
#


i_default_instances = 7


def launch_wow_instances(i_instances=i_default_instances):
    print("Instances to launch: " + str(i_instances))


i_default_instances = 3

launch_wow_instances()


# With a mutable object
def add_instance(s_instance_uuid, a_instances=[]):
    a_instances.append(s_instance_uuid)

    return a_instances

print(add_instance("267f2d5a-8930-11ea-bea8-2b67fbe5ab05"))
print(add_instance("2733dac0-8930-11ea-906f-9b6e24c72f0b"))
print(add_instance("278c9b56-8930-11ea-9539-efd8d07f289a"))
