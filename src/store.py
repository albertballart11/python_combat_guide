#!/usr/bin/env python3
#
# Example of Base Class and Extending it by subclasses
#
# Author: Carles Mateo
# Creation Date: 2017-06-10
# Description: Simple example of OOP to simplify multicountry e-Commerce
#


class Store:

    COUNTRY_CODE = "WW"
    COUNTRY_NAME = "World"

    PAYMENT_VISA = "VISA"
    PAYMENT_MASTERCARD = "MASTERCARD"
    PAYMENT_AMERICANEXPRESS = "AMERICANEXPRESS"

    def payment_methods(self):
        return self.PAYMENT_VISA, self.PAYMENT_MASTERCARD, self.PAYMENT_AMERICANEXPRESS

    def return_country(self):
        return self.COUNTRY_CODE, self.COUNTRY_NAME


class StoreCatalonia(Store):

    COUNTRY_CODE = "CA"
    COUNTRY_NAME = "Catalonia"

class StoreUS(Store):

    COUNTRY_CODE = "US"
    COUNTRY_NAME = "United States"

class StoreBrazil(Store):

    COUNTRY_CODE = "BR"
    COUNTRY_NAME = "Brazil"

    PAYMENT_VISAPARCELADOS = "VISAPARCELADOS"

    def payment_methods(self):
        return self.PAYMENT_VISA, self.PAYMENT_AMERICANEXPRESS, self.PAYMENT_VISAPARCELADOS


# The natural flow would be to check the domain requested by the visitor and decide which Country Store to show.
o_store = Store()
print(o_store.return_country())
print("Supported Payment Methods")
for s_payment in o_store.payment_methods():
    print(s_payment)
print("-"*50)

o_store_catalonia = StoreCatalonia()
print(o_store_catalonia.return_country())
print("Supported Payment Methods")
for s_payment in o_store_catalonia.payment_methods():
    print("* " + s_payment)
print("-"*50)

o_store_brazil = StoreBrazil()
print(o_store_brazil.return_country())
print("Supported Payment Methods")
for s_payment in o_store_brazil.payment_methods():
    print(s_payment)
print("-"*50)
