#!/usr/bin/env python3
#
# print_numbers_simple.py
#
# Author: Carles Mateo
# Creation Date: 2020-04-07
# Description: Show easy solution for problem found in Hackerrank.
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#
# Based on https://www.hackerrank.com/challenges/python-print/problem
#

if __name__ == '__main__':
    i_n = int(input())

    for i_number in range(1, i_n + 1):
        print(i_number, end='')