from mydisk import MyDisk


class TestMyDisk:

    def test_get_root_space_in_bytes(self):
        # We get ans instance of MyDisk class
        o_mydisk = MyDisk()

        i_total_size, i_free_size = o_mydisk.get_root_space_in_bytes()
        # Assert that type is Integer
        assert isinstance(i_total_size, int) is True
        assert isinstance(i_free_size, int) is True

        # Assert that values are consistent
        assert i_total_size >= 0
        assert i_free_size >= 0
        assert i_free_size <= i_total_size

    def test_get_root_free_space_in_multiple_units(self):
        # We get ans instance of MyDisk class
        o_mydisk = MyDisk()

        i_total_size_in_bytes, i_free_size_in_kbytes, i_free_size_in_mbytes, i_free_size_in_gbytes = \
            o_mydisk.get_root_free_space_in_multiple_units()

        # Assert that type is Integer
        assert isinstance(i_total_size_in_bytes, int) is True
        assert isinstance(i_free_size_in_kbytes, int) is True
        assert isinstance(i_free_size_in_mbytes, int) is True
        assert isinstance(i_free_size_in_gbytes, int) is True
