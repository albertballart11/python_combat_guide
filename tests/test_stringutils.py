from stringutils import StringUtils

class TestStringUtils:

    def test_get_format_string_from_number(self):
        o_stringutils = StringUtils()

        s_text_fmt = o_stringutils.get_format_string_from_number(0.1)
        assert s_text_fmt == "0.10"

        s_text_fmt = o_stringutils.get_format_string_from_number(1000)
        assert s_text_fmt == "1000.00"

        s_text_fmt = o_stringutils.get_format_string_from_number(3.14162032)
        assert s_text_fmt == "3.14"

        s_text_fmt = o_stringutils.get_format_string_from_number(0)
        assert s_text_fmt == "0.00"

        s_text_fmt = o_stringutils.get_format_string_from_number(-11.1)
        assert s_text_fmt == "-11.10"
