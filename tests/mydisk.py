#!/usr/bin/env python
#
# mydisk.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: Sample file to show the tests. Is stored in tests/ folder on purpose as part of the lessons progress.
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

import os


class MyDisk:

    i_root_total_size_in_bytes = None

    def get_root_space_in_bytes(self):
        """
        Return the size of the / mountpoint and the freespace
        :return: int: i_total_size
        :return: int: i_free_size
        """
        o_result = os.statvfs('/')
        i_block_size = o_result.f_frsize
        i_total_blocks = o_result.f_blocks
        i_free_blocks = o_result.f_bfree

        i_total_size = i_total_blocks * i_block_size
        i_free_size = i_free_blocks * i_block_size

        return i_total_size, i_free_size

    def get_root_free_space_in_multiple_units(self):
        """
        Will return the Free Space in / in multiply units.
        Please note: This method is a sample to teach Unit Testing and decimals are not treated on purpose
                     in order to have some interesting cases to deal with.
        :return: int: i_free_size_in_bytes
        :return: int: i_free_size_in_kbytes
        :return: int: i_free_size_in_mbytes
        :return: int: i_free_size_in_gbytes

        """
        i_total_size_in_bytes, i_free_size_in_bytes = self.get_root_space_in_bytes()
        i_free_size_in_kbytes = int(i_free_size_in_bytes / 1024)
        i_free_size_in_mbytes = int(i_free_size_in_kbytes / 1024)
        i_free_size_in_gbytes = int(i_free_size_in_mbytes / 1024)

        return i_free_size_in_bytes, i_free_size_in_kbytes, i_free_size_in_mbytes, i_free_size_in_gbytes

    def get_root_free_space_in_tbytes(self):
        """
        Return the Free Space in Terabytes
        :return: int: i_root_free_size_in_tbytes
        """

        i_root_total_size_in_bytes, i_root_free_space_in_bytes = self.get_root_space_in_bytes()

        i_root_free_size_in_tbytes = int(i_root_free_space_in_bytes / 1024 / 1024 / 1024 / 1024)

        return i_root_free_size_in_tbytes

    def get_root_space_in_tbytes(self):
        """
        Return the Total Space in Terabytes
        :return: int: i_root_total_size_in_tbytes
        """

        # This is created on purpose to explain problems with this approach
        if isinstance(self.i_root_total_size_in_bytes, None):
            i_root_total_size_in_bytes, i_root_free_space_in_bytes = self.get_root_space_in_bytes()
        else:
            i_root_total_size_in_bytes = self.i_root_total_size_in_bytes

        i_root_total_size_in_tbytes = int(i_root_total_size_in_bytes / 1024 / 1024 / 1024 / 1024)

        return i_root_total_size_in_tbytes
