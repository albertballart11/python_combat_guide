#
# test_sleeputils.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: Tests for the class SleepUtils
# URL: https://gitlab.com/carles.mateo/python_combat_guide/-/blob/master/tests/test_sleeputils.py
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

import pytest
import time
from lib.sleeputils import SleepUtils


@pytest.fixture
def do_not_sleep(monkeypatch):
    def sleep(i_seconds):
        pass

    monkeypatch.setattr(time, 'sleep', sleep)


class TestSleepUtils:

    def test_rest(self, do_not_sleep):
        o_sleeputils = SleepUtils()

        o_sleeputils.rest(i_seconds=60)

    def test_test_with_global_fixture(self, nosleep):
        o_sleeputils = SleepUtils()

        o_sleeputils.rest(i_seconds=60)
